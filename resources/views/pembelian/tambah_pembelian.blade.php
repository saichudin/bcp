
@extends('layouts.app')

@section('content')

<div class="container">

	<h2> Tambah Pembelian </h2>

	<form class="form-horizontal" method='post' action='tambah_pembelian'>
		 {{csrf_field()}}
	   <div class="row">
	  <div class="col-sm-5">
		 <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Kode</label>
			<div class="col-sm-8">
				@if (isset($details))
			  <input type="text" class="form-control" id='kode' name='kode' placeholder="" value="{{ $kode }}" readonly >
				@else
				<input type="text" class="form-control" id='kode' name='kode' placeholder="" value="{{ $lastid->kode_pembelian }}" readonly>

				@endif
			</div>
		 </div>

		 <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Tanggal (yyyy\mm\dd)</label>
			<div class="col-sm-8">
			  <input type="text" class="date form-control" id='tanggal' name='tanggal' value="@if (isset($tanggal)) {{$tanggal}} @endif" autocomplete="off">
			</div>
		 </div>

		 <div class="form-group">
			 	<label for="inputEmail3" class="col-sm-4 control-label">Kode Supplier</label>
	 			<div class="col-sm-8">
	 		  <select id='supplier' name='supplier' class='form-control'>
				@if (isset($supplier))
				<option value='{{$supplier}}'> {{$supplier}} &nbsp; </option>
				@endif
	 			<option value=''>- select -&nbsp;</option>
	 			@foreach($suppliers as $supplier)
	 			<option value='{{$supplier['kode_supplier']}}'>{{$supplier['kode_supplier']}} &nbsp;</option>
	 			@endforeach
	 			</select>
 		</div>
		 </div>

		 <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label"> Total Harga</label>
			<div class="col-sm-8">

				<?php $total = 0 ; ?>
				@if (isset($details))
				@foreach($details as $detail)
			  <?php
				$n = $detail->harga_satuan * $detail->jumlah;
				$total += $n;
				?>
				@endforeach
				@endif
			  <input type="text" class="form-control" id='harga' name='harga' placeholder="" value="{{$total}}" required readonly>
			</div>
		 </div>


</div>
</div>

<table  class="table" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
	 <thead>
		 <tr >
			 <th width='350'>Barang</th>
			 <th width='150'>Harga Satuan</th>
			 <th width='100'>Jumlah</th>
			 <th width='350'></th>
		 </tr>
	 </thead>
	 <tbody >
		 <tr bgcolor="#fff" >
		 <td align='center'>
		 <select id='barang' name='barang' class='form-control' >
		 <option value=''>- select -&nbsp;</option>
		 @foreach($barangs as $barang)
		 <option data-price='{{$barang['harga_beli']}}' value='{{$barang['kode_barang']}}'>{{$barang['kode_barang']}} &nbsp; {{$barang['nama_barang']}} &nbsp; {{$barang['harga_satuan']}} &nbsp;</option>
		 @endforeach
		 </select>
	 	 </td>
		 <td align='center'><input type="text" class="form-control" id='harga_satuan' name='harga_satuan' placeholder="" value=""  readonly></td>
			<td align='center'><input type="text" class="form-control" id='jumlah' name='jumlah' placeholder="" value=""  ></td>
		 <td>
			<button type="submit" class="btn btn-sm btn-primary">Tambah</button>
		 </td>

		 </tr>

		 </tbody>
		 </table>

		 <table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		 		<thead>
		 		<tr >
		 			<th style="color:white;" bgcolor="#666666" width='150'>Kode Barang</th>
		 			<th style="color:white;" bgcolor="#666666" width='150'>Harga Satuan</th>
		 			<th style="color:white;" bgcolor="#666666" width='300'>Jumlah</th>
		       <th style="color:white;" bgcolor="#666666" width='350'>Sub Total</th>
		 			<th style="color:white;" bgcolor="#666666" width='350'>action</th>

		 		</tr>
		 		</thead>
				<tbody >
					@if (isset($details))
					@foreach($details as $detail)

					<tr bgcolor="#fff" >
					<td align='center'>{{$detail->kode_barang}}</td>
					<td align='center'>{{$detail->harga_satuan}}</td>
		      <td align='center'>{{$detail->jumlah}}</td>
		      <td align='center'><?php echo $detail->harga_satuan * $detail->jumlah;  ?> </td>
					<td align='center'>
						<a href="" title="Back">view</a>

					</td>

					</tr>
					@endforeach
					@endif

					</tbody>
		 	</table>

		 <div class="form-group">
		 <div class="col-sm-1 col-sm-4">
			 <button type="submit" class="btn btn-sm btn-primary">Save</button>
			 <a href='javascript:history.back()' class='btn btn-sm btn-primary' >Cancel</a>
		 </div>
		 </div>

	</form>
</div>



@endsection
