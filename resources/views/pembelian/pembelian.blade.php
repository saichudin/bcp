
@extends('layouts.app')



@section('content')
<div class="container">
<h2> Pembelian (menambah stok)</h2>
<a href="{{ url('/tambah_pembelian') }}" ><button class="btn btn-success btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Tambah Pembelian</button></a>
<br><br>

<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		<thead>
		<tr >
			<th style="color:white;" bgcolor="#666666" width='150'>Kode Pembelian</th>
			<th style="color:white;" bgcolor="#666666" width='250'>Tanggal</th>
			<th style="color:white;" bgcolor="#666666" width='200'>Kode Supplier</th>
      <th style="color:white;" bgcolor="#666666" width='350'>Harga</th>
			<th style="color:white;" bgcolor="#666666" width='350'>action</th>

		</tr>
		</thead>
		<tbody >
			@foreach($datas as $data)

			<tr bgcolor="#fff" >
			<td align='center'>{{$data->kode_pembelian}}</td>
			<td align='center'>{{$data->tanggal_pembelian}}</td>
      <td align='center'>{{$data->kode_supplier}}</td>
      <td align='center'>{{$data->total_biaya}}</td>
			<td align='center'>
				<a href="{{ action('PembelianController@detail_pembelian', $data->kode_pembelian)}}" >view</a>

			</td>

			</tr>
			@endforeach

			</tbody>
			</table>


</div>

@endsection
