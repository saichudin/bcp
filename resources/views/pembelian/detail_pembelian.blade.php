
@extends('layouts.app')



@section('content')
<div class="container">

	<h2> Detail Pembelian </h2>

	<table class="table">
		<tbody>
			<tr>
				<td style="text-align: left; width: 15%;font-weight:bold;">Kode Pembelian</td>
				<td style="text-align: left; width: 2%;"><b>:</b></td>
				<td style="text-align: left; width: 33%;">{{$datas->kode_pembelian}} </td>
			</tr>
			<tr>
				<td style="text-align: left; width: 15%;font-weight:bold;">Tanggal</td>
				<td style="text-align: left; width: 2%;"><b>:</b></td>
				<td style="text-align: left; width: 33%;">{{$datas->tanggal_pembelian}}</td>
			</tr>
			<tr>
				<td style="text-align: left; width: 15%;font-weight:bold;">Kode Supplier</td>
				<td style="text-align: left; width: 2%;"><b>:</b></td>
				<td style="text-align: left; width: 33%;">{{$datas->kode_supplier}}</td>
			</tr>
			<tr>
				<td style="text-align: left; width: 15%;font-weight:bold;">Total Harga</td>
				<td style="text-align: left; width: 2%;"><b>:</b></td>
				<td style="text-align: left; width: 33%;">{{$datas->total_biaya}}</td>
			</tr>
		</tbody>
	</table>

<h5> Detail barang </h5>

	<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		 <thead>
		 <tr >
			 <th style="color:white;" bgcolor="#666666" width='150'>Kode Barang</th>
			 <th style="color:white;" bgcolor="#666666" width='150'>Harga Satuan</th>
			 <th style="color:white;" bgcolor="#666666" width='300'>Jumlah</th>
				<th style="color:white;" bgcolor="#666666" width='350'>Sub Total</th>
			 <th style="color:white;" bgcolor="#666666" width='350'>action</th>

		 </tr>
		 </thead>
		 <tbody >

			 @foreach($details as $detail)

			 <tr bgcolor="#fff" >
			 <td align='center'>{{$detail->kode_barang}}</td>
			 <td align='center'>{{$detail->harga_satuan}}</td>
			 <td align='center'>{{$detail->jumlah}}</td>
			 <td align='center'><?php echo $detail->harga_satuan * $detail->jumlah;  ?> </td>
			 <td align='center'>
				 <a href="" title="Back">view</a>

			 </td>

			 </tr>
			 @endforeach

			 </tbody>
	 </table>

<a href='javascript:history.back()' class='btn btn-sm btn-warning' >Back</a>
</div>

@endsection
