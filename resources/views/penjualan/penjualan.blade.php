
@extends('layouts.app')



@section('content')
<div class="container">
<h2> Penjualan (mengurangi stok)</h2>
<a href="{{ url('/tambah_penjualan') }}" ><button class="btn btn-success btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Tambah Penjualan</button></a>
<br><br>

<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		<thead>
		<tr >
			<th style="color:white;" bgcolor="#666666" width='150'>Kode Penjualan</th>
			<th style="color:white;" bgcolor="#666666" width='250'>Tanggal</th>
			<th style="color:white;" bgcolor="#666666" width='200'>Kode Pelanggan</th>
      <th style="color:white;" bgcolor="#666666" width='350'>Harga</th>
			<th style="color:white;" bgcolor="#666666" width='350'>action</th>

		</tr>
		</thead>
		<tbody >
			@php  $no = ($datas->currentpage()-1)* $datas->perpage() + 1;  @endphp
			@foreach($datas as $data)

			<tr bgcolor="#fff" >
			<td align='center'>{{$data->kode_penjualan}}</td>
			<td align='center'>{{$data->tanggal_penjualan}}</td>
      <td align='center'>{{$data->kode_pelanggan}}</td>
      <td align='center'>{{$data->total_biaya}}</td>
			<td align='center'>
			<a href="{{ action('PenjualanController@detail_penjualan', $data->kode_penjualan)}}" >view</a>
			</td>

			</tr>

			@php $no++; @endphp
			@endforeach

			</tbody>
			</table>
		{{ $datas->links() }}


</div>

@endsection
