
@extends('layouts.app')



@section('content')
<div class="container">

	<h2> Tambah Pembelian </h2>

  @if (isset($datas->kode_barang))
  <form class="form-horizontal" method='post' action='{{ action('BarangController@update', $datas->kode_barang) }}'>
  @else
	<form class="form-horizontal" method='post' action='tambah_barang'>
  @endif
		 {{csrf_field()}}
	   <div class="row">
	  <div class="col-sm-5">
		 <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Nama Barang</label>
			<div class="col-sm-8">
				@if (isset($datas->kode_barang))
				<input type="text" class="form-control" id='nama' name='nama' placeholder="" value="{{$datas->nama_barang}}" required>
				@else
				<input type="text" class="form-control" id='nama' name='nama' placeholder="" value="" required>
				@endif
			</div>
		 </div>

		 <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Deskripsi</label>
			<div class="col-sm-8">
			  <textarea class="form-control" rows="5" id='desc' name='desc'>@if (isset($datas->kode_barang)){{$datas->deskripsi_barang}}@endif</textarea>
			</div>
		 </div>

     <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Harga Beli</label>
			<div class="col-sm-8">
				@if (isset($datas->kode_barang))
				<input type="text" class="form-control" id='harga' name='harga' placeholder="" value="{{$datas->harga_beli}}" required>
				@else
				<input type="text" class="form-control" id='harga' name='harga' placeholder="" value="" required>
				@endif
			</div>
		 </div>

		 <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label">Harga Jual</label>
			<div class="col-sm-8">
				@if (isset($datas->kode_barang))
				<input type="text" class="form-control" id='harga_jual' name='harga_jual' placeholder="" value="{{$datas->harga_jual}}" required>
				@else
				<input type="text" class="form-control" id='harga_jual' name='harga_jual' placeholder="" value="" required>
				@endif
			</div>
		 </div>



  </div>
  </div>

		 <div class="form-group">
		 <div class="col-sm-1 col-sm-4">
			 <button type="submit" class="btn btn-sm btn-primary">Save</button>
			 <a href='javascript:history.back()' class='btn btn-sm btn-primary' >Cancel</a>
		 </div>
		 </div>

	</form>
</div>

@endsection
