
@extends('layouts.app')



@section('content')
<div class="container">

<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		<thead>
		<tr >
			<th style="color:white;" bgcolor="#666666" width='150'>Kode Pelanggan</th>
			<th style="color:white;" bgcolor="#666666" width='150'>Nama Pelanggan</th>
			<th style="color:white;" bgcolor="#666666" width='300'>Telp</th>
      <th style="color:white;" bgcolor="#666666" width='350'>Alamat</th>
		</tr>
		</thead>
		<tbody >

			@foreach($datas as $data)

			<tr bgcolor="#fff" >
			<td align='center'>{{$data->kode_supplier}}</td>
			<td align='center'>{{$data->nama_supplier}}</td>
      <td align='center'>{{$data->no_telp_supplier}}</td>
      <td align='center'>{{$data->alamat_supplier}}</td>
			</tr>

			@endforeach

			</tbody>
			</table>


</div>

@endsection
