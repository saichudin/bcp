
@extends('layouts.app')



@section('content')
<div class="container">
<h2> Master Barang </h2>
<a href="{{ url('/tambah_barang') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Tambah Barang</button></a>
<br><br>
<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		<thead>
		<tr >
			<th style="color:white;" bgcolor="#666666" width='150'>Kode Barang</th>
			<th style="color:white;" bgcolor="#666666" width='350'>Nama Barang</th>
			<th style="color:white;" bgcolor="#666666" width='400'>Deskripsi</th>
      <th style="color:white;" bgcolor="#666666" width='150'>Harga Beli</th>
			<th style="color:white;" bgcolor="#666666" width='150'>Harga Jual</th>
			<th style="color:white;" bgcolor="#666666" width='150'>Stok</th>
			<th style="color:white;" bgcolor="#666666" width='150'>Action</th>


		</tr>
		</thead>
		<tbody >
			@php  $no = ($datas->currentpage()-1)* $datas->perpage() + 1;  @endphp
			@foreach($datas as $data)

			<tr bgcolor="#fff" >
			<td align='center'>{{$data->kode_barang}}</td>
			<td align='center'>{{$data->nama_barang}}</td>
      <td align='center'>{{$data->deskripsi_barang}}</td>
      <td align='center'>{{$data->harga_beli}}</td>
			<td align='center'>{{$data->harga_jual}}</td>
			<td align='center'>{{$data->stoks}}</td>
			<td align='center'>
				<span style="float:left;"><a href='edit_barang/{{$data->kode_barang}}' class='btn btn-primary btn-xs' >edit</a>
	      </span> |
				<span style="float:right;">
				<form class="form-horizontal" method='post' action='{{ action('BarangController@destroy', $data->kode_barang) }}'>
				<input type="hidden" name="_method" value="delete" />
	    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Delete This record ?')" >Delete</button>
				</form>
				</span>
			</td>
			</tr>

			@php $no++; @endphp
			@endforeach

			</tbody>
			</table>
		{{ $datas->links() }}


</div>

@endsection
