
@extends('layouts.app')



@section('content')
<div class="container">
<h2> Master User </h2>
<form class="form-horizontal" method='post' action='input_user'>
	 {{csrf_field()}}
   <div class="row">
  <div class="col-sm-5">
	 <div class="form-group">
		<label for="inputEmail3" class="col-sm-4 control-label">Username *</label>
		<div class="col-sm-5">
		  <input type="text" class="form-control" id='username' name='username' placeholder="" value="{{ old('username') }}" required>
		</div>
	 </div>

  <div class="form-group">
  <label for="inputEmail3" class="col-sm-4 control-label">Email *</label>
  <div class="col-sm-5">
    <input type="email" class="form-control" id='email' name='email' placeholder=""  value="{{ old('email') }}" required>
  </div>
   </div>

   <div class="form-group">
   <label for="inputEmail3" class="col-sm-4 control-label">Password *</label>
   <div class="col-sm-5">
     <input type="password" class="form-control" id='password' name='password' placeholder="" required>
   </div>
  </div>

  <div class="form-group">
  <label for="inputEmail3" class="col-sm-4 control-label">confirm Password *</label>
  <div class="col-sm-5">
    <input type="password" class="form-control" id='password_confirmation' name='password_confirmation' placeholder="" required>
  </div>
  </div>

  </div>
</div>


    <div class="form-group">
		<div class="col-sm-offset-1 col-sm-2">
		  <button type="submit" class="btn btn-sm btn-primary">Save</button>
		</div>
	  </div>

</form>
<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		<thead>
		<tr >
			<th style="color:white;" bgcolor="#666666" width='40'>Nr.</th>
			<th style="color:white;" bgcolor="#666666" width='150'>Username</th>
			<th style="color:white;" bgcolor="#666666" width='300'>Email</th>
      <th style="color:white;" bgcolor="#666666" width='350'>Action</th>
		</tr>
		</thead>
		<tbody >
			@php  $no = ($users->currentpage()-1)* $users->perpage() + 1;  @endphp
			@foreach($users as $user)

			<tr bgcolor="#fff" >
			<td align='center'>{{$no}}</td>
			<td align='center'>{{$user->name}}</td>
      <td align='center'>{{$user->email}}</td>
			<td align='center' width='15%'>


			</td>
			</tr>

			@php $no++; @endphp
			@endforeach

			</tbody>
			</table>
		{{ $users->links() }}


</div>

@endsection
