
@extends('layouts.app')



@section('content')
<div class="container">

<table  class="table table-bordered" id='mt' width='100%' height="" data-toggle="dataTable" data-form="deleteForm">
		<thead>
		<tr >
			<th style="color:white;" bgcolor="#666666" width='150'>Kode Pelanggan</th>
			<th style="color:white;" bgcolor="#666666" width='150'>Nama Pelanggan</th>
			<th style="color:white;" bgcolor="#666666" width='300'>Telp</th>
      <th style="color:white;" bgcolor="#666666" width='350'>Alamat</th>
		</tr>
		</thead>
		<tbody >
			@php  $no = ($datas->currentpage()-1)* $datas->perpage() + 1;  @endphp
			@foreach($datas as $data)

			<tr bgcolor="#fff" >
			<td align='center'>{{$data->kode_pelanggan}}</td>
			<td align='center'>{{$data->nama_pelanggan}}</td>
      <td align='center'>{{$data->no_telp_pelanggan}}</td>
      <td align='center'>{{$data->alamat_pelanggan}}</td>
			</tr>

			@php $no++; @endphp
			@endforeach

			</tbody>
			</table>
		{{ $datas->links() }}


</div>

@endsection
