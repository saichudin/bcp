<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/input_user', 'UserController@index')->name('user');
Route::post('/input_user', 'UserController@add');

Route::get('/pelanggan', 'PelController@index')->name('pelanggan');

Route::get('/barang', 'BarangController@index')->name('barang');
Route::get('/tambah_barang','BarangController@tambah');
Route::post('/tambah_barang','BarangController@store');
Route::get('/edit_barang/{id}','BarangController@edit');
Route::post('/edit_barang/{id}','BarangController@update');
Route::delete('/barang/{id}','BarangController@destroy');

Route::get('/supplier', 'SupplierController@index')->name('supplier');

Route::get('/pembelian', 'PembelianController@index')->name('pembelian');
Route::get('/tambah_pembelian', 'PembelianController@tambah_pembelian');
Route::post('/tambah_pembelian', 'PembelianController@store');
Route::get('/detail_pembelian/{kode}', 'PembelianController@detail_pembelian');


Route::get('/penjualan', 'PenjualanController@index')->name('penjualan');
Route::get('/tambah_penjualan', 'PenjualanController@tambah_penjualan');
Route::post('/tambah_penjualan', 'PenjualanController@store');
Route::get('/detail_penjualan/{kode}', 'PenjualanController@detail_penjualan');

Route::get('/mutasi_stok', 'ReportController@index')->name('mutasi_stok');





//unused



Route::get("/admin", function(){
   return View::make("admin.dashboard");
});

Route::get('/inventory', 'InventoryController@index')->name('inventory');
Route::get('/inventory/add', 'InventoryController@add');
Route::post('/inventory/add', 'InventoryController@store');
Route::get('/inventory/view/{id}', 'InventoryController@view');
Route::get('/inventory/edit/{id}', 'InventoryController@edit');
Route::post('/inventory/{id}', 'InventoryController@update');
Route::delete('/inventory/{id}', 'InventoryController@delete');
