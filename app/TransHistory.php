<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransHistory extends Model
{
    //
    public $incrementing = false;
    protected $table = 'trans_history';
    public $timestamps = false;
}
