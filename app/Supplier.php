<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    public $incrementing = false;
    protected $table = 'master_supplier';
    public $timestamps = false;
    protected $primaryKey = 'kode_supplier';
}
