<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    //
    public $incrementing = false;
    protected $table = 'pembelian';
    public $timestamps = false;
    protected $primaryKey = 'kode_pembelian';
}
