<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPembelian extends Model
{
    //
    public $incrementing = false;
    protected $table = 'detail_pembelian';
    public $timestamps = false;
    protected $primaryKey = 'kode_pembelian';
}
