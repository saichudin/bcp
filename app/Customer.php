<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    public $incrementing = false;
    protected $table = 'master_pelanggan';
    public $timestamps = false;
    protected $primaryKey = 'kode_pelanggan';
}
