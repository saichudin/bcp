<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    //
    public $incrementing = false;
    protected $table = 'stok';
    public $timestamps = false;
    protected $primaryKey = 'kode_barang';
}
