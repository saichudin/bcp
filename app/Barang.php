<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    public $incrementing = false;
    protected $table = 'master_barang';
    public $timestamps = false;
    protected $primaryKey = 'kode_barang';
}
