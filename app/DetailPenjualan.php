<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenjualan extends Model
{
    //
    public $incrementing = false;
    protected $table = 'detail_penjualan';
    public $timestamps = false;
    protected $primaryKey = 'kode_penjualan';
}
