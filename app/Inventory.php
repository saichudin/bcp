<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    //
    protected $table = 'inventory';
    protected $primaryKey = 'id';
    protected $fillable = ['item','stock','price'];
}
