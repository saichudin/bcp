<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    //
    public $incrementing = false;
    protected $table = 'penjualan';
    public $timestamps = false;
    protected $primaryKey = 'kode_penjualan';
}
