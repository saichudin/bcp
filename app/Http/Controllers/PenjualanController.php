<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penjualan;
use App\DetailPenjualan;
use App\Customer;
use App\Barang;
use App\TransHistory;
use App\Stok;
use DB;
use Auth;

class PenjualanController extends Controller
{
    //
    private $now ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->now = date('Y-m-d H:i:s');
    }

    public function index()
    {
       $datas = \App\Penjualan::where('dibuat_oleh','!=','draft')->orderBy('kode_penjualan')->paginate(10);
       return view('penjualan/penjualan', compact('datas'));
    }

    public function tambah_penjualan()
    {
      $kode = Penjualan::orderBy('kode_penjualan','DESC')->first();
      if ($kode)
      $id = ++$kode->kode_penjualan;
      else
      $id = 'TJ001';

      //membaut id pembelian dg status draft
      $isi_id = new Penjualan();
      $isi_id->kode_penjualan = $id;
      $isi_id->dibuat_oleh = 'draft';
      $isi_id->tanggal_dibuat = $this->now;
      $isi_id->save();

      $lastid = Penjualan::orderBy('kode_penjualan','DESC')->first();

      $custs = \App\Customer::orderBy('kode_pelanggan')->get();
      $barangs = \App\Barang::orderBy('kode_barang')->get();
      return view('penjualan/tambah_penjualan',compact('custs','barangs','lastid'));
    }

    public function store(Request $request)
        {
          $kode_barang = $request->barang;

          if (isset($kode_barang))
          {
            //menmabahkan detail barang pembelian
            $detail = new DetailPenjualan();
            $detail->kode_penjualan = $request->kode;
            $detail->kode_barang = $request->barang;
            $detail->harga_satuan = $request->harga_satuan;
            $detail->jumlah = $request->jumlah;
            $detail->status = 'draft';
            $detail->save();

            $stok = Stok::where('kode_barang',$request->barang)->first();

            $history = new TransHistory();
            $history->kode_transaksi = $request->kode;
            $history->jenis = 'jual';
            $history->kode_barang = $request->barang;
            $history->jumlah = $request->jumlah;
            $history->total_harga = $request->harga_satuan * $request->jumlah;
            $history->stok_sebelum = $stok->stok;
            $history->stok_sesudah = $stok->stok - $request->jumlah;
            $history->tanggal = $this->now;
            $history->status = 'draft';
            $history->save();

            $details= DetailPenjualan::where('kode_penjualan',$request->kode)->get();
            $kode = $request->kode;
            $custs = \App\Customer::orderBy('kode_pelanggan')->get();
            $barangs = \App\Barang::orderBy('kode_barang')->get();

            $tanggal = $request->tanggal;
            $cust = $request->customer;

            return view('penjualan/tambah_penjualan',compact('custs','barangs','details','kode','tanggal','cust'));
          }
          else{

          //$barang = Barang::where('kode_barang','=',$kode_barang)->first();

          //$total = $request->jumlah * $barang->harga_satuan;

          //Jika list barang sudah selesai lalu klik save utk update detail pembelian
          $dibuat = Auth::user()->name;
          $data = Penjualan::find($request->kode);
          $data->tanggal_penjualan =  $request->tanggal;
          $data->kode_pelanggan =  $request->customer;
          $data->total_biaya =  $request->harga;
          $data->tanggal_dibuat = $this->now;
          $data->dibuat_oleh =  $dibuat;
          $data->save();

          //tambah history dan update stok
          $history = TransHistory::where('kode_transaksi',$request->kode)->get();
          foreach($history as $hist)
          {
            $stok = Stok::find($hist->kode_barang);
            $stok->stok = $stok->stok - $hist->jumlah;
            $stok->lastupdate = $this->now;
            $stok->save();
          }

          $update_d = DetailPenjualan::where('kode_penjualan',$request->kode)
                    ->update([
                        'status' => 'updated',
                    ]);

          $update = TransHistory::where('kode_transaksi',$request->kode)
                    ->update([
                        'status' => 'updated',
                    ]);
          /*
          $detail = new DetailPembelian();
          $detail->kode_pembelian = $request->kode;
          $detail->kode_barang = $kode_barang;
          $detail->harga_satuan = $barang->harga_satuan;
          $detail->jumlah = $request->jumlah;
          $detail->save();
          */
          //menambah Stok

          /*
          $stok = Barang::find($kode_barang);
          $stok->stok = $stok->stok + $request->jumlah;
          $stok->save();
          */
          return redirect('/penjualan');
          }
       }
    public function detail_penjualan($id)
    {
      $datas = Penjualan::find($id);
      $details = DetailPenjualan::where('kode_penjualan',$id)->get();
      return view('penjualan/detail_penjualan', compact('datas','details'));
    }
}
