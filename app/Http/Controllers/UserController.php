<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $users = \App\User::orderBy('name')->paginate(10);
       return view('master/input_user', compact('users'));
    }

    public function add(Request $request)
        {

          $this->validate($request, [
              'username' => 'required|min:2|max:50',
              'email' => 'email',
              'password' => 'required|confirmed|min:3',
          ]);


        	$user = new User();
        try {
    		$user->name =  $request->input('username');
        $user->email =  $request->input('email');
        $user->password =  bcrypt($request->input('password'));
        $user->save();
        	return redirect('/input_user');
    		 }
    		catch(\Exception $e)
    		 {return redirect()->back()->with('data',$e->getMessage()); }
       }
}
