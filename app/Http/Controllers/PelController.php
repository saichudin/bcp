<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelanggan;

class PelController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $datas = \App\Pelanggan::orderBy('kode_pelanggan')->paginate(10);
       return view('master/pelanggan', compact('datas'));
    }
}
