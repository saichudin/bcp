<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembelian;
use App\DetailPembelian;
use App\Supplier;
use App\Barang;
use App\TransHistory;
use App\Stok;
use DB;
use Auth;

class PembelianController extends Controller
{
    private $now ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->now = date('Y-m-d H:i:s');
    }

    public function index()
    {
       $datas = \App\Pembelian::where('dibuat_oleh','!=','draft')->orderBy('kode_pembelian')->paginate(10);
       return view('pembelian/pembelian', compact('datas'));
    }

    public function tambah_pembelian()
    {
      $kode = Pembelian::orderBy('kode_pembelian','desc')->first();
      if ($kode)
      $id = ++$kode->kode_pembelian;
      else
      $id = 'TB001';

      //membaut id pembelian dg status draft
      $isi_id = new Pembelian();
      $isi_id->kode_pembelian = $id;
      $isi_id->dibuat_oleh = 'draft';
      $isi_id->tanggal_dibuat = $this->now;
      $isi_id->save();

      $lastid = Pembelian::orderBy('kode_pembelian','DESC')->first();

      $suppliers = \App\Supplier::orderBy('kode_supplier')->get();
      $barangs = \App\Barang::orderBy('kode_barang')->get();
      return view('pembelian/tambah_pembelian',compact('suppliers','barangs','lastid'));
    }

    public function store(Request $request)
        {
          $kode_barang = $request->barang;

          if (isset($kode_barang))
          {
            //menmabahkan detail barang pembelian
            $detail = new DetailPembelian();
            $detail->kode_pembelian = $request->kode;
            $detail->kode_barang = $request->barang;
            $detail->harga_satuan = $request->harga_satuan;
            $detail->jumlah = $request->jumlah;
            $detail->status = 'draft';
            $detail->save();

            $stok = Stok::where('kode_barang',$request->barang)->first();

            $history = new TransHistory();
            $history->kode_transaksi = $request->kode;
            $history->jenis = 'beli';
            $history->kode_barang = $request->barang;
            $history->jumlah = $request->jumlah;
            $history->total_harga = $request->harga_satuan * $request->jumlah;
            $history->stok_sebelum = $stok->stok;
            $history->stok_sesudah = $stok->stok + $request->jumlah;
            $history->tanggal = $this->now;
            $history->status = 'draft';
            $history->save();

            $details= DetailPembelian::where('kode_pembelian',$request->kode)->get();
            $kode = $request->kode;
            $suppliers = \App\Supplier::orderBy('kode_supplier')->get();
            $barangs = \App\Barang::orderBy('kode_barang')->get();

            $tanggal = $request->tanggal;
            $supplier = $request->supplier;

            return view('pembelian/tambah_pembelian',compact('suppliers','barangs','details','kode','tanggal','supplier'));
          }
          else{

          //$barang = Barang::where('kode_barang','=',$kode_barang)->first();

          //$total = $request->jumlah * $barang->harga_satuan;

          //Jika list barang sudah selesai lalu klik save utk update detail pembelian
          $dibuat = Auth::user()->name;
          $data = Pembelian::find($request->kode);
          $data->tanggal_pembelian =  $request->tanggal;
          $data->kode_supplier =  $request->supplier;
          $data->total_biaya =  $request->harga;
          $data->tanggal_dibuat = $this->now;
          $data->dibuat_oleh =  $dibuat;
          $data->save();

          //tambah history dan update stok
          $history = TransHistory::where('kode_transaksi',$request->kode)->get();
          foreach($history as $hist)
          {
            $stok = Stok::find($hist->kode_barang);
            $stok->stok = $stok->stok + $hist->jumlah;
            $stok->lastupdate = $this->now;
            $stok->save();
          }

          $update_d = DetailPembelian::where('kode_pembelian',$request->kode)
                    ->update([
                        'status' => 'updated',
                    ]);

          $update = TransHistory::where('kode_transaksi',$request->kode)
                    ->update([
                        'status' => 'updated',
                    ]);
          /*
          $detail = new DetailPembelian();
          $detail->kode_pembelian = $request->kode;
          $detail->kode_barang = $kode_barang;
          $detail->harga_satuan = $barang->harga_satuan;
          $detail->jumlah = $request->jumlah;
          $detail->save();
          */
          //menambah Stok

          /*
          $stok = Barang::find($kode_barang);
          $stok->stok = $stok->stok + $request->jumlah;
          $stok->save();
          */
          return redirect('/pembelian');
          }
       }

    public function detail_pembelian($id)
    {
      $datas = Pembelian::find($id);
      $details = DetailPembelian::where('kode_pembelian',$id)->get();
      return view('pembelian/detail_pembelian', compact('datas','details'));
    }
}
