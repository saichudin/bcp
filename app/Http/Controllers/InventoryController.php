<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;

class InventoryController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index ()
    {
        $datas = Inventory::all();
        return view('inventory.inventory', compact('datas'));
    }

    public function add ()
    {
        return view('inventory.add');
    }

    public function store (Request $request)
    {
        $invent = new Inventory();
        $invent->item = $request->item;
        $invent->stock = $request->stock;
        $invent->price = $request->price;
        $invent->save();

        return redirect('/inventory');
    }

    public function view ($id)
    {
        $datas = Inventory::FindorFail($id);
        return view('inventory.view', compact('datas'));
    }

    public function edit ($id)
    {
        $datas = Inventory::FindorFail($id);
        return view ('inventory.edit', compact('datas'));
    }

    public function update (Request $request, $id)
    {
        $datas = Inventory::FindorFail($id);
        $datas->item = $request->item;
        $datas->stock = $request->stock;
        $datas->price = $request->price;
        $datas->save();

        return redirect('inventory')->with('flash_message', 'Inventory updated!');
    }

    public function delete($id)
    {
        Inventory::destroy($id);
        return redirect('inventory')->with('flash_message','Inventory deleted!');
    }
}
