<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Stok;
use DB;

class BarangController extends Controller
{
    private $now ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->now = date('Y-m-d H:i:s');
    }

    public function index()
    {
      /* $datas = DB::table('master_barang')
                ->join('stok','master_barang.kode_barang','=','stok.kode_barang')
                ->select('master_barang.*','stok.stok as stoks')
                ->paginate(10); */
       $datas = Barang::join('stok','master_barang.kode_barang','=','stok.kode_barang')
                ->select('master_barang.*','stok.stok as stoks')
                ->paginate(10);
      // $datas = \App\Barang::orderBy('kode_barang')->paginate(10);
       return view('master/barang', compact('datas'));
    }

    public function tambah()
    {
      return view('master/tambah_barang');
    }

    public function store(Request $request)
    {
      $kode = Barang::orderBy('kode_barang','desc')->first();
      $id_b = ++$kode->kode_barang;

      $datas = new Barang();
      $datas->kode_barang = $id_b;
      $datas->nama_barang = $request->nama;
      $datas->deskripsi_barang = $request->desc;
      $datas->harga_beli = $request->harga;
      $datas->harga_jual = $request->harga_jual;
      //$datas->stok = $request->stok;
      $datas->save();

      $stok = new Stok();
      $stok->kode_barang = $id_b;
      $stok->stok = 0;
      $stok->lastupdate = $this->now;
      $stok->save();

      return redirect('/barang');
    }

    public function edit($id)
    {
      $datas = Barang::find($id);
      return view('master/tambah_barang',compact('datas'));
    }

    public function update(Request $request,$id)
    {
      $datas = Barang::find($id);
      $datas->nama_barang = $request->nama;
      $datas->deskripsi_barang = $request->desc;
      $datas->harga_satuan = $request->harga;
      $datas->stok = $request->stok;
      $datas->save();

      return redirect('/barang');
    }

    public function destroy($id)
    {
      $datas = Barang::find($id);
      $datas->delete();
      return redirect('/barang');
    }
}
